$(function() {
  'use strict';
  if (!window.console) window.console = {};
  if (!window.console.memory) window.console.memory = function() {};
  if (!window.console.debug) window.console.debug = function() {};
  if (!window.console.error) window.console.error = function() {};
  if (!window.console.info) window.console.info = function() {};
  if (!window.console.log) window.console.log = function() {};

  // sticky footer
  //-----------------------------------------------------------------------------
  if (!Modernizr.flexbox) {
    (function() {
      var
        $pageWrapper = $('#page-wrapper'),
        $pageBody = $('#page-body'),
        noFlexboxStickyFooter = function() {
          $pageBody.height('auto');
          if ($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
            $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
          } else {
            $pageWrapper.height('auto');
          }
        };
      $(window).on('load resize', noFlexboxStickyFooter);
    })();
  }
  if (ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
    (function() {
      var
        $pageWrapper = $('#page-wrapper'),
        $pageBody = $('#page-body'),
        ieFlexboxFix = function() {
          if ($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
            $pageWrapper.height($(window).height());
            $pageBody.removeClass('flex-none');
          } else {
            $pageWrapper.height('auto');
          }
        };
      ieFlexboxFix();
      $(window).on('load resize', ieFlexboxFix);
    })();
  }

  // placeholder
  //-----------------------------------------------------------------------------
  $('input[placeholder], textarea[placeholder]').placeholder();

  //Pop Up

  $('.js-show-pop-up').on('click', function(e) {
    e.preventDefault();
    $('.pop-up, .overlay').fadeIn('fast');
  });

  $('.js-close-pop-up, .overlay').on('click', function(e) {
    e.preventDefault();
    closePop();
  });

  $('body').keydown(function(eventObject) {
    if (eventObject.which === 27) {
      closePop();
    }
  });

  function closePop() {
    $('.pop-up, .overlay').fadeOut('fast');
  }

  //Canvas

  var canvas = new fabric.Canvas('canvas');
  canvas.setWidth(638);
  canvas.setHeight(440);
  $('#file_input').hide();

  var loaded_image;
  var background_image;
  var loaded_image2;

  function readURL(input) {
    console.log(input);

    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(input.files[0]);

      reader.onload = function(event) {
        var imgObj = new Image();
        event = event || window.event;
        imgObj.src = event.target.result;
        loaded_image = new fabric.Image(imgObj);
        loaded_image.name = "back";
        loaded_image2 = new fabric.Image();
        loaded_image2.name = "front";

        if (loaded_image.width === 0) {
          setTimeout(function() {
            readURL(input);
          }, 1000);
        }

        var difer = loaded_image.width / 300;
        loaded_image.width = 300;
        loaded_image.height = loaded_image.height / difer;
        loaded_image2.width = loaded_image.width;
        loaded_image2.height = loaded_image.height;
        loaded_image.selectable = false;
        loaded_image.top = 200;
        loaded_image.left = 350;
        loaded_image2.top = 200;
        loaded_image2.left = 350;
        loaded_image2.transparentCorners = false;
        loaded_image2.centeredScaling = true;
        loaded_image2.padding = 1;
        loaded_image2.borderColor = '#993300';
        loaded_image2.cornerColor = '#993300';
        loaded_image.cornerSize = 6;
        loaded_image.transparentCorners = false;
        loaded_image.centeredScaling = true;
        loaded_image.padding = 1;
        loaded_image.borderColor = '#993300';
        loaded_image.cornerColor = '#993300';
        loaded_image.cornerSize = 6;
        loaded_image.originX = 150;
        loaded_image.originY = 150;
        loaded_image2.originX = 150;
        loaded_image2.originY = 150;
        canvas.clear().add(loaded_image).add(background_image).add(loaded_image2);
        canvas.renderAll();
        canvas.setActiveObject(loaded_image2);

        // var asp = loaded_image.width/loaded_image.height;
      }
    } else {
      // console.log('else');
    }
  }

  $('#file_input').change(function() {

    $('.preview .alert').fadeOut('slow');

    var fileSize = this.files[0].size;
    var fileType = this.files[0].type;
    var maxFileSize = 4 * 1024 * 1000;

    if (fileType != 'image/jpeg' && fileType != 'image/png') {
      $('.preview').prepend('<div class="alert">Фотография должна быть в формате Jpeg или Png.</div>');
      // setTimeout(function(){
      //   $('.preview .alert').fadeOut('slow');
      // },1200);
    } else if (fileSize > maxFileSize) {
      $('.preview').prepend('<div class="alert">Фотография должна быть не более чем 4 мб.</div>');
      // setTimeout(function(){
      //   $('.preview .alert').fadeOut('slow');
      // },1200);
    } else {
      $('#file_input').hide();
      $('.input-btn-stl').hide();
      readURL(this);
    }



  });

  var input = $('.input-btn-stl');

  $('.patern').click(function(e) {
    $('#file_input').val('');
    e.preventDefault();

    var eq = $(this).index(length);

    switch (eq) {
      case 0:
        input.css({
          'left': '31.6em',
          'top': '10em'
        })
        break;

      case 1:
        input.css({
          'left': '29em',
          'top': '12em'
        })

        break;
      case 2:
        input.css({
          'left': '23em',
          'top': '15em'
        })

        break;
      case 3:
        input.css({
          'left': '26em',
          'top': '11em'
        })

        break;

      default:
        break;
    }

    $('#file_input').show();
    $('.input-btn-stl').show();
    $('.patern').removeClass('active');
    $(this).addClass('active');

    background_image = new fabric.Image($(this).find('img')[0]);
    background_image.height = canvas.height;
    background_image.width = canvas.width;
    background_image.hasControls = background_image.hasBorders = background_image.selectable = false;
    canvas.clear().add(background_image);
    canvas.renderAll();
  });

  $('.patern.active img').one("load", function() {
    // do stuff
    background_image = new fabric.Image($('.patern.active').find('img')[0]);
    background_image.height = canvas.height;
    background_image.width = canvas.width;
    background_image.hasControls = background_image.hasBorders = background_image.selectable = false;
    canvas.clear().add(background_image);
    canvas.renderAll();
    $('#file_input').show();
    $('.input-btn-stl').show();
    input.css({
      'left': '31.6em',
      'top': '10em'
    });
  }).each(function() {
    if (this.complete) $(this).load();
  });

  $('#to_back').click(function(e) {
    e.preventDefault();
    canvas.clear();
    canvas.add(loaded_image);
    canvas.add(background_image);
    canvas.add(loaded_image2);
    canvas.renderAll();
    canvas.setActiveObject(loaded_image);
  });

  $('#to_front').click(function(e) {
    e.preventDefault();
    canvas.clear();
    canvas.add(background_image);
    canvas.add(loaded_image);
    canvas.renderAll();
  });

  canvas.on({
    'object:modified': function(element) {
      if (loaded_image2) {
        loaded_image.top = loaded_image2.top;
        loaded_image.left = loaded_image2.left;
        loaded_image.height = loaded_image2.height;
        loaded_image.width = loaded_image2.width;
        loaded_image.scaleX = loaded_image2.scaleX;
        loaded_image.scaleY = loaded_image2.scaleY;
        // loaded_image.setOpacity(0.8);
      }
    },

    // 'mouse:move': function() {
    //   if (loaded_image2) {
    //     if (canvas.getActiveObject() == loaded_image2) {
    //       loaded_image.setOpacity(1);
    //     } else {
    //       loaded_image.setOpacity(0.8);
    //     }
    //   }
    // },
    'object:rotating': function(element) {
      if (loaded_image2) {
        loaded_image.originX = loaded_image.width / 2;
        loaded_image.originY = loaded_image.height / 2;
        loaded_image.top = loaded_image2.top;
        loaded_image.left = loaded_image2.left;
        loaded_image.height = loaded_image2.height;
        loaded_image.width = loaded_image2.width;
        loaded_image.scaleX = loaded_image2.scaleX;
        loaded_image.scaleY = loaded_image2.scaleY;
        if (loaded_image2.angle) {
          loaded_image.angle = loaded_image2.angle;
        }
      }
    },
    'object:moving': function(element) {
      if (loaded_image2) {
        loaded_image.originX = 0;
        loaded_image.originY = 0;
        loaded_image2.originX = 0;
        loaded_image2.originY = 0;
        loaded_image.top = loaded_image2.top;
        loaded_image.left = loaded_image2.left;
        loaded_image.height = loaded_image2.height;
        loaded_image.width = loaded_image2.width;
        loaded_image.scaleX = loaded_image2.scaleX;
        loaded_image.scaleY = loaded_image2.scaleY;
        // loaded_image.setOpacity(0.8);
      }
    },
  });
  $("#save_button").click(function() {
    canvas.setActiveObject(background_image);
  });
});
