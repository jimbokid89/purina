<?php
$fileName = $_FILES["file_input"]["name"]; // The file name
$fileTmpLoc = $_FILES["file_input"]["tmp_name"]; // File in the PHP tmp folder
$fileType = $_FILES["file_input"]["type"]; // The type of file it is
$fileSize = $_FILES["file_input"]["size"]; // File size in bytes
$fileErrorMsg = $_FILES["file_input"]["error"]; // 0 for false... and 1 for true
if (!$fileTmpLoc) { // if file not chosen
    echo "ERROR: Please browse for a file before clicking the upload button.";
    exit();
}
if(move_uploaded_file($fileTmpLoc, "test_uploads/$fileName")){
    echo "$fileName upload is complete";
} else {
    echo "move_uploaded_file function failed";
}
?>
